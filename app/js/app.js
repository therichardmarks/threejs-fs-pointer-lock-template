// app.js
// Fullscreen THREE.js Template
// (C) 2014, Richard Marks

// make sure that threejs is loaded
var THREE = THREE || {};

// this extension to THREEjs allows a fast scene cleanup
THREE.Scene.prototype.clear = function () {
    var children = this.children;
    // we only empty the array if it exists and contains at least a single element
    if ( children !== undefined && children !== null && children.length > 0 ) {
        // this has been tested and proven to be the most efficient 
        // method to empty an array in JavaScript
        while ( children.length > 0 ) {
            children.pop();
        }
    }
    // return this to allow chaining
    return this;
};

// the entire application lives within the app global object.
// all variables and functions are created within this object.
// this allows easy inspection and manipulation from the console
// without polluting the global scope with hundreds to thousands 
// of functions, objects, and variables.
var app = {
    // threejs objects
    renderer: null,
    scene: null,
    camera: null,
    clock: null,
    container: null,
    controls: null,
    
    // used for pausing the application when the window loses focus
    hasFocus: true,
    
    // camera variables
    width: 0, // width height and aspect are configured at runtime for full-screen
    height: 0,
    aspect: 0,
    fovAngle: 45,
    nearPlane: 0.1,
    farPlane: 10000.0,
    cameraStart: { x: 0, y: 0, z: 300 }
};

// initialize the application
app.init = function ( gameContainerDOMElementID ) {
    // use the full browser size as our render surface
    app.width = window.innerWidth;
    app.height = window.innerHeight;
    app.aspect = app.width / app.height;
    
    // create the renderer
    app.renderer = new THREE.WebGLRenderer();
    app.renderer.setSize( app.width, app.height );
    
    // attach the renderer to the DOM
    app.container = document.getElementById( gameContainerDOMElementID );
    app.container.appendChild( app.renderer.domElement );

    // create the camera
    app.camera = new THREE.PerspectiveCamera( app.fovAngle, app.aspect, app.nearPlane, app.farPlane );
    app.camera.position.set( app.cameraStart.x, app.cameraStart.y, app.cameraStart.z );
    
    // create the scene
    app.scene = new THREE.Scene();
    
    // create the clock
    app.clock = new THREE.Clock();
    
    // create the controls
    app.controls = new THREE.PointerLockControls( app.camera );
    
    // set up the focus handlers
    window.onblur = function () { app.lost_focus(); };
    window.onfocus = function () { app.gained_focus(); };
    
    // we want to handle the scenario where the user resizes the window
    // and maintain the proper aspect ratio
    window.addEventListener( 'resize', function () { 
        app.window_resized(); 
    }, false );
    
    // pointer lock code
    (function () {
        app.pointer_lock = {
            blocker: document.getElementById( 'blocker' ),
            instructions: document.getElementById( 'instructions' )
        };
        
        var havePointerLock = 'pointerLockElement' in document || 'mozPointerLockElement' in document || 'webkitPointerLockElement' in document;
        
		if ( havePointerLock ) {
			var element = document.body;
			var pointerlockchange = function ( event ) {
				if ( document.pointerLockElement === element || 
				     document.mozPointerLockElement === element || 
				     document.webkitPointerLockElement === element ) {
					app.controls.enabled = true;
					app.pointer_lock.blocker.style.display = 'none';
				} else {
					app.controls.enabled = false;
					app.pointer_lock.blocker.style.display = '-webkit-box';
					app.pointer_lock.blocker.style.display = '-moz-box';
					app.pointer_lock.blocker.style.display = 'box';
					app.pointer_lock.instructions.style.display = '';
				}
			};
            
			var pointerlockerror = function ( event ) {
				app.pointer_lock.instructions.style.display = '';
			};
            
			// Hook pointer lock state change events
			document.addEventListener( 'pointerlockchange', pointerlockchange, false );
			document.addEventListener( 'mozpointerlockchange', pointerlockchange, false );
			document.addEventListener( 'webkitpointerlockchange', pointerlockchange, false );
			document.addEventListener( 'pointerlockerror', pointerlockerror, false );
			document.addEventListener( 'mozpointerlockerror', pointerlockerror, false );
			document.addEventListener( 'webkitpointerlockerror', pointerlockerror, false );
            
			app.pointer_lock.instructions.addEventListener( 'click', function ( event ) {
				app.pointer_lock.instructions.style.display = 'none';
				// Ask the browser to lock the pointer
				element.requestPointerLock = element.requestPointerLock || element.mozRequestPointerLock || element.webkitRequestPointerLock;
				if ( /Firefox/i.test( navigator.userAgent ) ) {
					var fullscreenchange = function ( event ) {
						if ( document.fullscreenElement === element || 
						     document.mozFullscreenElement === element || 
						     document.mozFullScreenElement === element ) {
							document.removeEventListener( 'fullscreenchange', fullscreenchange );
							document.removeEventListener( 'mozfullscreenchange', fullscreenchange );
							element.requestPointerLock();
						}
					};
					
					document.addEventListener( 'fullscreenchange', fullscreenchange, false );
					document.addEventListener( 'mozfullscreenchange', fullscreenchange, false );
					element.requestFullscreen = element.requestFullscreen || element.mozRequestFullscreen || element.mozRequestFullScreen || element.webkitRequestFullscreen;
					element.requestFullscreen();
				} else {
					element.requestPointerLock();
				}
			}, false );
		} else {
			app.pointer_lock.instructions.innerHTML = "Your browser doesn't seem to support Pointer Lock API";
		}
    })();
};

// create the application content
app.create = function () {
    // here we create our content collection objects for easy manipulation from the console.
    // content collections will be stored in an object instead of an array so that the content can be
    // indexed by name instead of by a numerical index.
    app.content = {
        lights: {},
        geometries: {},
        materials: {},
        meshes: {}
    };
    
    app.create_lights = function () {
        app.content.lights = {}; // reset collection
        
        // the color of ambient lights gets applied to all objects in the scene globally
        app.content.lights.ambient = {};
        app.content.lights.ambient.white = new THREE.AmbientLight( 0x101010 );
        
        // directional lights can be thought of as lights with a powerful source such as the sun
        app.content.lights.directional = {};
        app.content.lights.directional.sun = new THREE.DirectionalLight( 0xffffff, 1 );
    };
    
    app.create_geometries = function () {
        app.content.geometries = {}; // reset collection
        app.content.geometries.cube = new THREE.BoxGeometry( 10, 10, 10 );
    };
    
    app.create_materials = function () {
        app.content.materials = {}; // reset collection
        
        // basic materials (for flat-shaded objects)
        app.content.materials.basic = {};
        app.content.materials.basic.green = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
        
        // lambertian materials (for smooth-shaded non-shiny objects)
        app.content.materials.lambert = {};
        app.content.materials.lambert.red = new THREE.MeshLambertMaterial( { color: 0xff0000 } );
        
        // phong materials (for shiny objects)
        app.content.materials.phong = {};
        app.content.materials.phong.red = new THREE.MeshPhongMaterial( { 
            ambient: 0x030303, 
            color: 0xff0000, 
            specular: 0x905000, 
            shininess: 30, 
            shading: THREE.FlatShading
        } );
    };
   
    app.create_meshes = function () {
        app.content.meshes = {}; // reset collection
        
        var geometry = app.content.geometries.cube;
        var material = app.content.materials.lambert.red;
        
        app.content.meshes.cube = new THREE.Mesh( geometry, material );
        
        // lots of random positioned cubes make up the world
        app.content.meshes.world = [];
        for ( var i = 0; i < 500; i++ ) {
            material = app.content.materials.basic.green;
            var mesh = new THREE.Mesh ( geometry, material );
            mesh.position.x = Math.floor( Math.random() * 20 - 10 ) * 20;
			mesh.position.y = Math.floor( Math.random() * 20 ) * 20 + 10;
			mesh.position.z = Math.floor( Math.random() * 20 - 10 ) * 20;
			app.content.meshes.world.push( mesh );
        }
    };
    
    // this is where we actually build the scene which will be displayed
    app.build_scene = function () {
        // reset the scene
        app.scene.clear();
        
        // add the controls object (VERY IMPORTANT)
        app.scene.add( app.controls.getObject() );
        
        // add our lights
        app.content.lights.directional.sun.position.set( 0.5, 0.5, 2.0 );
        app.scene.add( app.content.lights.directional.sun );
        app.scene.add( app.content.lights.ambient.white );
        
        // add our meshes
        app.scene.add( app.content.meshes.cube );
        
        for ( var i = 0; i < app.content.meshes.world.length; i++ ) {
            app.scene.add( app.content.meshes.world[ i ] );
        }
    };
    
    // here is the actual creation process
    (function () {
        app.create_lights();
        app.create_geometries();
        app.create_materials();
        app.create_meshes();
        app.build_scene();
    })();
};

// update a single frame of the application
app.update = function () {
    app.controls.update();
};

// render a single frame of the application
app.render = function () {
    app.renderer.render( app.scene, app.camera );
};

// called when the window loses focus
app.lost_focus = function () {
    app.hasFocus = false;
};

// called when the window gains focus
app.gained_focus = function () {
    app.hasFocus = true;
};

// called when the window is resized
app.window_resized = function () {
    app.width = window.innerWidth;
    app.height = window.innerHeight;
    app.aspect = app.width / app.height;
    app.renderer.setSize( app.width, app.height );
    app.camera.aspect = app.aspect;
    app.camera.updateProjectionMatrix();
};

// application boot sequence: init -> create -> start "heartbeat" loop
// we do this inside of a self-executing anonymous function to explicitly
// depict that this chunk of code executes one time and does not need to
// be exposed to the global scope, as it cannot be altered at runtime.
(function () {
    // 'game' is the id of the div in index.html in which the game will be housed
    app.init('game');
    app.create();
    
    // the main loop of the application runs at ~60 FPS
    function heartbeat () {
        window.requestAnimationFrame( heartbeat );
        if ( app.hasFocus ) {
            app.update();
        }
        app.render();
    }
    
    heartbeat();
})();