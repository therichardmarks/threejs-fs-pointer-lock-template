# Fullscreen THREE.js Template

Developed by Richard Marks <ccpsceo@gmail.com>

## Features

 + Starts Fullscreen
 + Pauses on Blur
 + Resumes on Focus
 + Procedural Design (global variables and global functions as opposed to encapsulated within namespaces, OOP, etc...)

## Dependencies
 + THREE.js (Included)
 + requestAnimationFrame Polyfill (Included)

## Project Organization

 + The *index.html* file is the entry point for the application.
 + The application's files are contained within the *app* folder tree.
 + The third-party dependencies are contained within the *vendor* folder tree.

### Application Folder Organization

 + The *css* folder contains the stylesheets for the application.
 + The *images* folder contains the images for the application.
 + The *js* folder contains the JavaScript files for the application.

(C) 2014, Richard Marks